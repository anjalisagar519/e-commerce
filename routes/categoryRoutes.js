const express = require('express')
const { getAllCategories, getCategoryById, addNewCategory, updateCategory, deleteCategory } = require('../controller/categoryController')
const router = express.Router()


// Get all categories-Get
router.get('/', getAllCategories)

//Get a category by Id -Get
router.get('/:categoryId', getCategoryById)

//Add a new category - Post
router.post('/', addNewCategory)

//update a new category - Put or Patch
router.patch('/:categoryId', updateCategory)

// delete a category - Delete
router.delete('/:categoryId', deleteCategory)

module.exports = router