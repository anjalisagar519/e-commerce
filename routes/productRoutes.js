const express = require('express')
const { getAllProducts, getProductById, addNewProduct, updateProduct, deleteaProduct } = require('../controller/productController')
const router = express.Router()


// Get all products -Get
router.get('/products', getAllProducts)

//Get a product by Id -Get
router.get('/products/:productId', getProductById)
//Add a new product - Post
router.post('/products', addNewProduct)

//update a new poduct - Put or Patch
router.patch('/products/:productId', updateProduct)

// delete a product  - Delete
router.delete('products/:productId', deleteaProduct)

module.exports = router